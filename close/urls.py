from django.contrib import admin
from django.urls import path, re_path, include
from rest_framework_jwt.views import obtain_jwt_token
import stocks.urls


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(stocks.urls)),
    path('jwt-auth/', obtain_jwt_token),
]
