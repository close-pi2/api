from django.test import TestCase
from datetime import datetime
import requests
from rest_framework.test import APIClient
import json


from .models import *


class TestPermissions(TestCase):

    def setUp(self):
        victor = User.objects.create_user(username="victornavarro", email="victor@gmail.com", password="omelete1")
        vitor = User.objects.create_user(username="vitorbarbosa", email="vitor@gmail.com", password="omelete1")
        nabeer = Beer.objects.create(user=victor, type_name="IPA", name="Nabeer", price=10.0)
        napabeer = Beer.objects.create(user=vitor, type_name="APA", name="Napa", price=12.0)
        victor_cycle = Cycle.objects.create(user=victor, beer=nabeer, start_time=datetime.now(), end_time=datetime.now(), beer_count=10)
        Log.objects.create(cycle=victor_cycle, code=1, ocurred_at=datetime.now())

    def test_not_being_able_to_access_without_token(self):
        base_url = "http://localhost:8000"
        endpoints = ["/users/1/beers/", "/users/1/cycles/"]

        for endpoint in endpoints:
            result = requests.get(base_url+endpoint)
            assert result.status_code == 401


    def test_should_get_valid_token(self):
        user = User.objects.get(username="victornavarro")
        client = APIClient()
        result = client.post('http://localhost:8000/jwt-auth/', {
            'username': 'victornavarro',
            'password': 'omelete1'
        })

        assert result.status_code == 200
        assert "token" in str(result.content)

    def test_should_access_with_token(self):
        base_url = "http://localhost:8000"
        endpoints = ["/users/1/beers/", "/users/1/cycles/"]
        client = APIClient()
        result = client.post(base_url+'/jwt-auth/', {
            'username': 'victornavarro',
            'password': 'omelete1'
        })
        token = json.loads(result.content.decode('utf-8'))["token"]

        for endpoint in endpoints:
            response = client.get(base_url + endpoint, {}, HTTP_AUTHORIZATION='JWT {}'.format(token))
            assert response.status_code == 200

    def test_should_not_access_even_with_token(self):
        base_url = "http://localhost:8000"
        endpoints = ["/cycles/", "/beers/", "/users/"]
        client = APIClient()
        result = client.post(base_url+'/jwt-auth/', {
            'username': 'victornavarro',
            'password': 'omelete1'
        })
        token = json.loads(result.content.decode('utf-8'))["token"]

        for endpoint in endpoints:
            response = client.get(base_url + endpoint, {}, HTTP_AUTHORIZATION='JWT {}'.format(token))
            assert response.status_code == 403

    def test_should_access_if_admin(self):
        base_url = "http://localhost:8000"
        endpoints = ["/cycles/", "/beers/", "/users/"]
        client = APIClient()
        user = User.objects.get(username="victornavarro")
        user.is_admin = True
        user.is_superuser = True
        user.is_staff = True
        user.save()
        result = client.post(base_url+'/jwt-auth/', {
            'username': 'victornavarro',
            'password': 'omelete1'
        })
        token = json.loads(result.content.decode('utf-8'))["token"]

        for endpoint in endpoints:
            response = client.get(base_url + endpoint, {}, HTTP_AUTHORIZATION='JWT {}'.format(token))
            assert response.status_code == 200