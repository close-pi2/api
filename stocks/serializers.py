from django.contrib.auth.models import User
from rest_framework import serializers
from stocks.models import Cycle, Beer, Log, Rasp


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 'cycles', 'beers', 'rasps')


class LogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Log
        fields = ('id', 'code', 'ocurred_at')


class CycleSerializer(serializers.ModelSerializer):
    logs = LogSerializer(many=True)

    class Meta:
        model = Cycle
        fields = ('id', 'beer', 'start_time', 'end_time', 'beer_count', 'logs')


class BeerSerializer(serializers.ModelSerializer):
    cycles = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Beer
        fields = ('id', 'type_name', 'name', 'price', 'cycles')

class RaspSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rasp
        fields = ('id', 'rasp_ip', 'created_at')
