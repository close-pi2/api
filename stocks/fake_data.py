from .models import *
from random import choice, randint, uniform, randrange
from datetime import datetime, date, time, timedelta

first_names = ["Victor", "Eduardo", "Andre", "Ivan", "Rodrigo"]
last_names = ["Navarro", "Soares", "Barbosa", "Bernardo", "Silva", "Pereira"]
email_add = ["2009", "cool"]
beer_types = ["IPA", "APA", "Red Ale", "Double IPA", "Sour"]

def random_date(start, end):
    """
    This function will return a random datetime between two datetime
    objects.
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)
    return start + timedelta(seconds=random_second)

for i in range(5):
    user = User.objects.create_user(first_name=choice(first_names),
        last_name=choice(last_names),
        email=choice(first_names)+choice(email_add)+"@gmail.com",
        password="qwe12345",
        username="user"+str(i))

    for k in range(10):
        beer = Beer.objects.create(user=user,
            type_name=choice(beer_types),
            name=user.first_name+"Beer",
            price=round(uniform(1.0, 20.0), 2))
        start_date = datetime.now() + timedelta(hours=k)
        end_date = start_date + timedelta(minutes=randint(1, 200))
        cycle = Cycle.objects.create(user=user, beer=beer, start_time=start_date, end_time=end_date, beer_count=randint(1, 60))

        for l in range(15):
            log = Log.objects.create(cycle=cycle, code=randint(0,7), ocurred_at=random_date(start_date, end_date))
