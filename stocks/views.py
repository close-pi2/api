from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework import permissions
from django.http import Http404


from stocks.models import *
from stocks.serializers import *
from stocks.permissions import IsOwner

class UserView(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_permissions(self):
        if self.action == 'list':
            permission_classes = [permissions.IsAdminUser]
        elif self.action == 'new':
            permission_classes = [permissions.AllowAny]
        elif self.action == 'detail':
            permission_classes = [permissions.IsAuthenticated, IsOwner]
        else:
            permission_classes = [permissions.IsAuthenticated]
        return [permission() for permission in permission_classes]

    def get_object(self):
        pk = self.kwargs['pk']
        if pk == 'me':
            return self.request.user
        else:
            return super().get_object()

    @action(methods=['post'], detail=True)
    def new_cycle(self, request, pk=None):
        owner = User.objects.get(id=request.user.id)
        cycle = Cycle(**request.data)
        cycle.user = owner
        cycle.save()

        return Response(CycleSerializer(cycle).data)

    @action(methods=['get'], detail=True)
    def cycles(self, request, pk=None):
        owner = User.objects.get(id=request.user.id)
        cycles = owner.cycles.all().order_by('-start_time')

        return Response(CycleSerializer(cycles, many=True).data)

    @action(methods=['get'], detail=True)
    def beers(self, request, pk=None):
        owner = User.objects.get(id=request.user.id)
        beers = owner.beers.all()

        return Response(BeerSerializer(beers, many=True).data)

    @action(methods=['post'], detail=False)
    def new(self, request, format=None):
        new_user = User.objects.create_user(**request.data)

        return Response(UserSerializer(new_user).data)

    @action(methods=['get'], detail=True)
    def last_cycle(self, request, pk=None):
        owner = User.objects.get(id=request.user.id)

        try:
            last_cycle = owner.cycles.latest('start_time')
        except Cycle.DoesNotExist:
            raise Http404("No last cycle.")

        return Response(CycleSerializer(last_cycle).data)

    @action(methods=['post'], detail=True)
    def new_rasp(self, request, pk=None):
        owner = User.objects.get(id=request.user.id)
        rasp = Rasp(**request.data)
        rasp.user = owner
        rasp.save()

        return Response(RaspSerializer(rasp).data)

    @action(methods=['get'], detail=True)
    def rasps(self, request, pk=None):
        owner = User.objects.get(id=request.user.id)
        rasps = owner.rasps.order_by('-created_at')

        return Response(RaspSerializer(rasps, many=True).data)

    @action(methods=['get'], detail=True)
    def last_rasp_ip(self, request, pk=None):
        owner = User.objects.get(id=request.user.id)
        rasp = owner.rasps.latest('created_at')

        return Response(RaspSerializer(rasp).data)

class CycleView(viewsets.ModelViewSet):
    queryset = Cycle.objects.all()
    serializer_class = CycleSerializer

    def get_permissions(self):
        if self.action == 'list':
            permission_classes = [permissions.IsAdminUser]
        elif self.action == 'detail':
            permission_classes = [permissions.IsAdminUser]
        else:
            permission_classes = [permissions.IsAuthenticated]
        return [permission() for permission in permission_classes]

    @action(methods=['post'], detail=True)
    def new_log(self, request, pk=None):
        new_log = Log(**request.data)
        new_log.cycle = Cycle.objects.get(id=pk)
        new_log.save()

        return Response(LogSerializer(new_log).data)

    @action(methods=['get'], detail=True)
    def cycle_logs(self, request, pk=None):
        logs = Log.objects.filter(cycle=pk).order_by('-ocurred_at')

        return Response(LogSerializer(logs, many=True).data)



class BeerView(viewsets.ModelViewSet):
    queryset = Beer.objects.all()
    serializer_class = BeerSerializer

    def get_permissions(self):
        if self.action == 'list':
            permission_classes = [permissions.IsAdminUser]
        elif self.action == 'detail':
            permission_classes = [permissions.IsAdminUser]
        elif self.action == 'new_cycle':
            permission_classes = [permissions.IsAuthenticated]
        else:
            permission_classes = [permissions.IsAuthenticated]
        return [permission() for permission in permission_classes]

    @action(methods=['post'], detail=False)
    def new(self, request, format=None):
        new_beer = Beer(**request.data)
        new_beer.user = request.user
        new_beer.save()

        return Response(BeerSerializer(new_beer).data)

    @action(methods=['post'], detail=True)
    def new_cycle(self, request, pk=None):
        new_cycle = Cycle(**request.data)
        new_cycle.beer = Beer.objects.get(id=pk)
        new_cycle.user = request.user
        new_cycle.save()

        return Response(CycleSerializer(new_cycle).data)

class RaspView(viewsets.ModelViewSet):
    queryset = Rasp.objects.all()
    serializer_class = RaspSerializer

    @action(methods=['post'], detail=False)
    def new_rasp(self, request, pk=None):
        new_rasp = Rasp(**request.data)
        new_rasp.user = request.user
