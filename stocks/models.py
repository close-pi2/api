from django.contrib.auth.models import User
from django.db import models
from datetime import datetime


class Beer(models.Model):
    user = models.ForeignKey(User, related_name='beers', on_delete=models.CASCADE)
    type_name = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    price = models.FloatField()

    def __unicode__(self):
        return '%s, %s' % (self.name, self.type_name)


class Cycle(models.Model):
    user = models.ForeignKey(User, related_name='cycles', on_delete=models.CASCADE)
    beer = models.ForeignKey(Beer, related_name='cycles', on_delete=models.PROTECT, null=True)
    start_time = models.DateTimeField(auto_now_add=True)
    end_time = models.DateTimeField(blank=True, null=True)
    beer_count = models.IntegerField(default=0)


class Log(models.Model):
    cycle = models.ForeignKey(Cycle, related_name='logs', on_delete=models.CASCADE)
    code = models.IntegerField()
    ocurred_at = models.DateTimeField(default=datetime.now)


class Rasp(models.Model):
    user = models.ForeignKey(User, related_name='rasps', on_delete=models.CASCADE)
    rasp_ip = models.CharField(max_length=15)
    created_at = models.DateTimeField(auto_now_add=True)