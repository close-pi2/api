from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from stocks import views

router = DefaultRouter()
router.register(r'cycles', views.CycleView)
router.register(r'beers', views.BeerView)
router.register(r'users', views.UserView)
router.register(r'rasps', views.RaspView)



urlpatterns = [
    url('^', include(router.urls))
]
